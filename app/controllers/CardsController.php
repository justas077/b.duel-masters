<?php

class CardsController extends \Phalcon\Mvc\Controller
{
    public function beforeExecuteRoute($dispatcher)
    {
        $this->view->disable();
    }

    public function createFromTxtAction() // POST
    {
        if($this->request->isPost()) {
            echo "isPost</br>";
            if($this->request->hasFiles()) {
                echo "hasFiles</br>";
                $path = '/storage/abc.txt';
                foreach ($this->request->getUploadedFiles(true) as $file) {
                    $file->moveTo($path);
                    echo $path."</br>";
                }
            }
        }

        echo "</br>KORTOS";
    }

}

