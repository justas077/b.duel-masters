<?php

use Phalcon\Mvc\Router;

// Create the router without default routes
$router = new Router(true);

$router->add(
    '/',
    [
        'controller' => 'index',
        'action'     => 'index',
    ]
);

$router->add(
    '/cards',
    [
        'controller' => 'cards',
        'action'     => 'index',
    ]
);