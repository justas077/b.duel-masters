<?php

class Cards extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $civilization;

    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var string
     */
    public $race;

    /**
     *
     * @var integer
     */
    public $cost;

    /**
     *
     * @var string
     */
    public $power;

    /**
     *
     * @var string
     */
    public $rules_text;

    /**
     *
     * @var string
     */
    public $flavor_text;

    /**
     *
     * @var string
     */
    public $rarity;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("duel-masters");
        $this->setSource("cards");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'cards';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cards[]|Cards|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Cards|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
